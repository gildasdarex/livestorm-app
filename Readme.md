This project is a test for livestorm

We implemented ecs-service-deployment module to deploy application in aws ecs cluster

This module will create the following resources in aws :

 - 1 vpc
 - 2 subnets (1 public subnet and 1 private subnet)
 - 1 route table
 - 1 internet gateway
 - ACL rule
 - 1 application load balancer
 - launch configuration
 -  iam roles
 -  iam policies
 - auto scaling group
 - security groups
 - Target group
 - AWS ECS cluster
 - EC2 instance
 
 


### REQUIREMENTS

| Name        | Version           
| ------------- |:-------------:|
| terraform      | = 0.13.4 |


### REQUIRED INPUT

| Name        | Version           
| ------------- |:-------------:|
| name      | Name of the cluster |
| region      | AWS region that should be used |
| tags      | tags that shoul be used for resources |
| availability_zones      | AWS availability zones where resources shouls be created |
| ssh_key_name      | ssh key name that you created in aws and will be used to ssh on EC2 instance |
| cluster_instance_type      | instance type that will be used for AWS ECS instance |
| task_definition      | json file that provide AWS ECS Task definition |
| family      | family for AWS ECS service and task definition |
| container_name      | container name for AWS ECS task definition |


Default values are provide for all thoses input in nginx test and rails app test but  ssh_key_name is required

How to use it :

 ## NOTE : WHEN YOU RUN THE TERRAFORM SCRIPT , A LOAD BALANCER WILL BE CREATED , YOU CAN USE THIS LOAD BALANCER TO ACCESS DIRECTLY TO APPLICATION THAT YOU DEPLOYED
 

### NGINX DEPLOYMENT EXAMPLE  

```  
For nginx test  task_definition file content is :
[
  {
    "name": "nginx",
    "image": "nginx:1.13-alpine",
    "essential": true,
    "portMappings": [
      {
        "containerPort": 80
      }
    ],
    "memoryReservation": 10,
    "cpu": 10
  }
]    
   
```   
```  
terraform {
  backend "s3" {
    region = "us-east-1"
    profile = "dev"
    bucket  = "tdarex.dev.ops"
    key     = "terraform/livestorm/dev/nginx/terraform.tfstate"
  }
}

provider "aws" {
  region = "${var.region}"
  profile = "${var.profile}"
}



module "ecs_service" {
  source              = "git::https://gitlab.com/gildasdarex/livestorm-app.git?ref=dev"
  name                = "${var.name}"
  region                = "${var.region}"
  tags                = "${var.tags}"
  availability_zones  = var.availability_zones
  ssh_key_name        = "${var.ssh_key_name}"
  cluster_instance_type        = "${var.cluster_instance_type}"
  task_definition    = var.task_definition
  family = var.family
  container_name = var.container_name
} 
```

1 - Create s3 bucket that will be used as backend to store terraform state file
    Use example in deployment-examples/s3(Update the default value in variables.tf file or provide a tfvars file)
    
```  
    cd deployment-examples/s3
    terraform init
    terraform plan
    terraform apply
```


2 - Deploy nginx example
    Update bucket value in deployment-examples/nginx/main.tf
    
```  
    cd deployment-examples/nginx
    terraform init
    terraform plan
    terraform apply
```


### RAILS APP DEPLOYMENT EXAMPLE
 Rails app code source and Dockerfile are located in rails-app-example/livestorm-app


```
terraform {
  backend "s3" {
    region = "us-east-1"
    profile = "dev"
    bucket  = "tdarex.dev.ops"
    key     = "terraform/livestorm/dev/railsapp/terraform.tfstate"
  }
}

provider "aws" {
  region = "${var.region}"
  profile = "${var.profile}"
}



module "ecs_service" {
  source              = "git::https://gitlab.com/gildasdarex/livestorm-app.git?ref=dev"
  name                = "${var.name}"
  region                = "${var.region}"
  tags                = "${var.tags}"
  availability_zones  = var.availability_zones
  ssh_key_name        = "${var.ssh_key_name}"
  cluster_instance_type        = "${var.cluster_instance_type}"
  task_definition    = var.task_definition
  family = var.family
  container_name = var.container_name
} 
```

1 - Create s3 bucket that will be used as backend to store terraform state file
    Use example in deployment-examples/s3(Update the default value in variables.tf file or provide a tfvars file)
     

```  
    cd deployment-examples/s3
    terraform init
    terraform plan
    terraform apply
    
```


2 - Create aws rds postgres database
    Update bucket value in deployment-examples/rails-app/main.tf
    
```  
    cd deployment-examples/postgres
    terraform init
    terraform plan
    terraform apply
```




3 - Deploy rails app
    Update bucket value in deployment-examples/rails-app/main.tf

``` 
    For rails test  task_definition file content is(Please value for DATABASE_URL to provide the right database url ) :
    [
      [
        {
          "name": "ruby-app",
          "image": "",
          "command": [
            "/bin/sh -c bundle exec rails db:migrate &&  bundle exec rails s -p 80 -b '0.0.0.0'"
          ],
          "entryPoint": [
            "sh",
            "-c"
          ],
          "essential": true,
          "portMappings": [
            {
              "containerPort": 80
            }
          ],
          "environment": [
            {
              "name": "PORT",
              "value" : "80"
            },
            {
              "name": "HOST",
              "value" : "localhost"
            },
            {
              "name": "ASSET_HOST",
              "value" : "http://localhost:80"
            },
            {
              "name": "DATABASE_URL",
              "value" : ""
            }
          ],
          "memoryReservation": 10,
          "cpu": 10
        }
      ]
    ]    
       
```
    
```  
    cd deployment-examples/rails-app
    terraform init
    terraform plan
    terraform apply
```

### NOTE : ONLY PORT 80 IS OPEN ON LOADBALANCER AFTER LODBALANCER

### IMPROV%ENTS
  - ADD ABILITY TO DEPLOY APP ON ANY LOAD BALANCER PORT
  - ADD PROMETHEUS EXPORTER TO RAILS APP