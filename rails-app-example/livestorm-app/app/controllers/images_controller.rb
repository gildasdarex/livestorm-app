class ImagesController < ApplicationController
  before_action :set_image, only: [:show, :destroy]

  def index
    ip = request.remote_ip
    @image = Image.new
    @images = Image.where(ip_address: ip)
  end

  def create
    @image = Image.new(photo: params[:photo])
    @image.ip_address = request.remote_ip
    respond_to do |format|
      if @image.save
        format.html{redirect_to images_path}
        format.json { render json: { new_image: render_to_string(partial: 'images/image.html', locals: {image: @image}) } }
      else
        @images = Image.where(ip_address: request.remote_ip)
        format.html {render :index, status: :unprocessable_entity}
        format.json { render json: { errors: @image.errors.full_messages }, status: :unprocessable_entity }
      end
    end
  end

  def show
    views_number = @image.views + 1
    @image.update!(views: views_number)
    image = open("#{@image.photo_url}")
    send_data image.read, filename: "#{@image.id}.#{@image.photo.file.extension}", type: "image/#{@image.photo.file.extension}", disposition: 'inline'
  end

  def destroy
    @image.destroy!
    respond_to do |format|
      format.json { head :no_content }
    end
  end


  private

  def set_image
    @image = Image.find params[:id]
  end
end
