module "ecs_cluster" {
  source              = "./modules/ecs-cluster"
  name                = "${var.name}"
  region                = "${var.region}"
  tags                = "${var.tags}"
  availability_zones  = var.availability_zones
  ssh_key_name        = "${var.ssh_key_name}"
  cluster_instance_type        = "${var.cluster_instance_type}"
}

module "alb" {
  source              = "./modules/application-load-balancer"
  name                = "${var.name}"
  hostname            = "${var.hostname}"
  domain_name         = "${var.domain}"
  service_port        = "${var.service_port}"
  service_protocol    = "${var.service_protocol}"
  service_sg_id       = "${module.ecs_cluster.security_group_id}"
  certificate_arn     = "${var.certificate_arn}"
  tags                = "${var.tags}"
  vpc_id              = "${module.ecs_cluster.vpc_id}"
  vpc_subnets         =  module.ecs_cluster.public_subnet_ids
}

resource "aws_iam_role" "svc" {
  name = "${var.name}-ecs-role"

  assume_role_policy = <<EOF
{
  "Version": "2008-10-17",
  "Statement": [
	{
	  "Sid": "",
	  "Effect": "Allow",
	  "Principal": {
		"Service": "ecs.amazonaws.com"
	  },
	  "Action": "sts:AssumeRole"
	}
  ]
}
EOF
}

resource "aws_iam_role_policy_attachment" "svc" {
  role       = "${aws_iam_role.svc.name}"
  policy_arn = "arn:aws:iam::aws:policy/service-role/AmazonEC2ContainerServiceRole"
}


resource "aws_ecs_task_definition" "app" {
  family = var.family
  container_definitions = file("${var.task_definition}")
}


resource "aws_ecs_service" "svc" {
  name            = "${var.service_name}"
  cluster         = "${var.name}"
  task_definition = "${aws_ecs_task_definition.app.arn}"
  desired_count   = "${var.desired_count}"
  iam_role        = "${aws_iam_role.svc.arn}"

  load_balancer {
    target_group_arn = "${module.alb.target_group_arn}"
    container_name   = "${var.container_name}"
    container_port   = "${var.container_port}"
  }
}