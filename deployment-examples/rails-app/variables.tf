variable "region" {
  description = "The region into which to deploy the cluster."
  type = string
  default     = "us-east-1"
}

variable "profile" {
  type        = "string"
  default     = "dev"
  description = "AWS CLI config profile"
}

variable "task_definition" {
  type        = "string"
  description = "AWS CLI config profile"
  default =  "task_definition.json"
}

variable "family" {
  type        = "string"
  description = " ECS task family"
  default = "livestorm-app"
}

variable "availability_zones" {
  type        = "list"
  default     = ["us-east-1a", "us-east-1b"]
  description = "Availability Zones for the cluster"
}

variable "tags" {
  type        = "map"
  default     = {
    "platform" = "mybu"
  }
  description = "Additional tags (e.g. `map('BusinessUnit`,`XYZ`)"
}


variable "name" {
  type        = "string"
  default     = "tdarex"
  description = "Solution name, e.g. 'app' or 'cluster'"
}



variable "cluster_instance_type" {
  description = "The instance type of the container instances."
  type = string
  default = "t2.medium"
}


variable "ssh_key_name" {
  type        = "string"
  default     = "ecs-nodes-key"
  description = "The ssh key that can be used to login to the cluster nodes"
}

variable "container_name" {
  default     = "ruby-app"
}

variable "container_port" {
  default     = 80
}