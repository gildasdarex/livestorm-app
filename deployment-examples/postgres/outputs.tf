output "address" {
  value = aws_db_instance.livestorm_db.address
}

output "endpoint" {
  value = aws_db_instance.livestorm_db.endpoint
}