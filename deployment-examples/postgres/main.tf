terraform {
  backend "s3" {
    region = "us-east-1"
    profile = "dev"
    bucket  = "tdarex.dev.ops"
    key     = "terraform/livestorm/dev/postgres/terraform.tfstate"
  }
}

provider "aws" {
  region = "${var.region}"
  profile = "${var.profile}"
}

resource "aws_db_instance" "livestorm_db" {
  allocated_storage    = 20
  storage_type         = "gp2"
  engine               = "postgres"
  engine_version       = "10.5"
  instance_class       = "db.t2.micro"
  name                 = "livestorm_db"
  username             = "livestorm"
  password             = "livestorm_password"
  publicly_accessible  = true
}