variable "region" {
  description = "The region into which to deploy the cluster."
  type = string
  default     = "us-east-1"
}

variable "profile" {
  type        = "string"
  default     = "dev"
  description = "AWS CLI config profile"
}