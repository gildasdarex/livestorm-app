terraform {
  backend "s3" {
    region = "us-east-1"
    profile = "dev"
    bucket  = "tdarex.dev.ops"
    key     = "terraform/livestorm/dev/nginx/terraform.tfstate"
  }
}

provider "aws" {
  region = "${var.region}"
  profile = "${var.profile}"
}



module "ecs_service" {
  source              = "git::https://gitlab.com/gildasdarex/livestorm-app.git?ref=dev"
  name                = "${var.name}"
  region                = "${var.region}"
  tags                = "${var.tags}"
  availability_zones  = var.availability_zones
  ssh_key_name        = "${var.ssh_key_name}"
  cluster_instance_type        = "${var.cluster_instance_type}"
  task_definition    = var.task_definition
  family = var.family
  container_name = var.container_name
}