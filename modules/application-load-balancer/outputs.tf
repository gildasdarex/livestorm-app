output "target_group_arn" {
  description = "ARN of the target group"
  value       = "${module.alb.target_group_arns[0]}"
}

output "this_lb_dns_name" {
  description = "ARN of the target group"
  value       = "${module.alb.this_lb_dns_name}"
}
