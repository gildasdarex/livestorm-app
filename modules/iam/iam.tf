resource "aws_iam_role" "cluster_instance_role" {
  description = "cluster-instance-role-${var.name}"
  assume_role_policy = file("${path.module}/cluster-instance-role.json")
  tags = var.tags
}

data "template_file" "cluster_instance_policy" {
  template = file(
  "${path.module}/cluster-instance-policy.json"
  )
}

resource "aws_iam_policy" "cluster_instance_policy" {
  description = "cluster-instance-policy-${var.name}"
  policy = coalesce(var.cluster_instance_iam_policy_contents, data.template_file.cluster_instance_policy.rendered)
}

resource "aws_iam_policy_attachment" "cluster_instance_policy_attachment" {
  name = "cluster-instance-policy-attachment-${var.name}"
  roles = [aws_iam_role.cluster_instance_role.id]
  policy_arn = aws_iam_policy.cluster_instance_policy.arn
}

resource "aws_iam_instance_profile" "cluster" {
  name = "cluster-instance-profile-${var.name}"
  path = "/"
  role = aws_iam_role.cluster_instance_role.name
}

resource "aws_iam_role" "cluster_service_role" {
  description = "cluster-service-role-${var.name}"
  assume_role_policy = file("${path.module}/cluster-service-role.json")
  tags = var.tags
}

resource "aws_iam_policy" "cluster_service_policy" {
  description = "cluster-service-policy-${var.name}"
  policy = coalesce(var.cluster_service_iam_policy_contents, file("${path.module}/cluster-service-policy.json"))
}

resource "aws_iam_policy_attachment" "cluster_service_policy_attachment" {
  name = "cluster-instance-policy-attachment-${var.name}"
  roles = [aws_iam_role.cluster_service_role.id]
  policy_arn = aws_iam_policy.cluster_service_policy.arn
}